# Cloud orchestration templates

[[_TOC_]]

## Description
In this project, we propose several templates for cloud orchestration and automation. We propose various scenarios and their related templates

## Proposed Architectures
### 1. [Three-Tier architecture on OpenStack](openstack/)
![Three-tier architecture](images/three-tier_openstack.png "Three-tier architecture on openstack")

### 2. [Deploy Apache Spark Cluster on OpenStack](apache_spark/)
This architecture helps to deploy quickly a spark cluster on openstack cloud. [Apache Spark](https://spark.apache.org/docs/latest/cluster-overview.html) is an open-source unified analytics engine for large-scale data processing. Spark provides an interface for programming entire clusters with implicit data parallelism and fault tolerance.
