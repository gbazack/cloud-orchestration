# Security group for bastion
resource "openstack_networking_secgroup_v2" "bastion-secgroup" {
    name        = "bastion-secgroup"
    description = "Security group for bastion instance"
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule-icmp-in" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "icmp"
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.bastion-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule-ssh" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 22
    port_range_max    = 22
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.bastion-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule-http" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 80
    port_range_max    = 80
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.bastion-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule-https" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 443
    port_range_max    = 443
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.bastion-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "bastion-rule-gitlab-runner" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 8093
    port_range_max    = 8093
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.bastion-secgroup.id
}

# Security group for server instance
resource "openstack_networking_secgroup_v2" "server-secgroup" {
    name        = "server-secgroup"
    description = "Security group for server instance"
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-icmp-in" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "icmp"
    remote_ip_prefix  = "192.168.1.0/24"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-ssh" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 22
    port_range_max    = 22
    remote_ip_prefix  = "192.168.1.0/24"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-http" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 80
    port_range_max    = 80
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-https" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 443
    port_range_max    = 443
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-mysql" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 3306
    port_range_max    = 3306
    remote_ip_prefix  = "192.168.1.0/24"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-frontend" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 8000
    port_range_max    = 8000
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "server-rule-backend" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 8001
    port_range_max    = 8001
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.server-secgroup.id
}