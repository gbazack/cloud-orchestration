#!/bin/bash
echo "Installing and configuring a Gitlab Runner"
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
usermod -aG sudo gitlab-runner
usermod -aG admin gitlab-runner
usermod -aG docker gitlab-runner
echo "gitlab-runner    ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start 
echo "Setting proxy for gitlab-runner service"
mkdir -p /etc/systemd/system/gitlab-runner.service.d/
touch /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
echo "[Service]" >> /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
echo "Environment=HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080" >> /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
echo "Environment=HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"  >> /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
echo "Environment=http_proxy=http://proxy.enst-bretagne.fr:8080"  >> /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
echo "Environment=https_proxy=http://proxy.enst-bretagne.fr:8080" >> /etc/systemd/system/gitlab-runner.service.d/http_proxy.conf
source /etc/environment   
systemctl daemon-reload
systemctl restart gitlab-runner.service
echo "Copying and changing permissions of ssh key"
cp -r /home/ubuntu/.ssh/ /home/gitlab-runner/
chown gitlab-runner:gitlab-runner -R /home/gitlab-runner/.ssh/
chmod 600 -R /home/gitlab-runner/.ssh/
echo "Registering a Gitlab Shell Runner"
gitlab-runner register --url https://gitlab.com/ --registration-token "${REGISTRATION_TOKEN}" --tag-list "${RUNNER_NAME}" --description "${RUNNER_DESCRIPTION}" --executor "${RUNNER_EXECUTOR}" --non-interactive
gitlab-runner restart
gitlab-runner verify